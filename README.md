# Spigot Phone #
Welcome to Spigot Phone! The plugin bringing mobile phones to your Minecraft Sever. 

## Planned Features: ##
* Messaging and Phone Call features (calling acting as a fast /msg feature, while messages acting as a /mail feature).
* SIM Cards (for different plans and phone numbers).
* PIN code and _TouchID_
* Mutiple phones for one player
* Same SIM for multiple phones
* Service towers (without signal, you are unable to send or recieve messages and phone calls).
* Address book to keep contacts (IGN, phone number, notes etc.).
* _Toggleable_ Use phones as the only contact method to other people - using data for services like Twitter for public chat, and you have to have service to message friends.
* _Toggleable_ Phones have limited battery, and once run out, must be charged
* _Possible_ Data Plans which allow you to warp to places and other features.
And more coming soon...

