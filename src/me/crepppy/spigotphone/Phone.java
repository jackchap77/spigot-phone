package me.crepppy.spigotphone;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class Phone {
    private Player owner;
    private int battery;
    private boolean passcodeEnabled;
    private int passcode;
    private List<AddressBookPlayer> addressBook;
    private boolean fingerPrintScanner;
    private boolean fingerPrintScannerEnabled;
    private SIM simCard;
    private boolean locked;
    private short service;
    private int id;

    private ItemStack item;
    private Inventory inv;

    public Phone(Player owner, boolean fingerPrintScanner) {
        id = ++SpigotPhone.CURRENT_ID;
        this.owner = owner;
        this.fingerPrintScanner = fingerPrintScanner;
        fingerPrintScannerEnabled = false;
        battery = 100;
        passcodeEnabled = false;
        addressBook = new ArrayList<>();
        locked = false;
        simCard = null;
        service = 4; // Remove once calculate service method complete
        updatePhone();
        int slot = -1;
        for (int i = 0; i < owner.getInventory().getContents().length; i++) {
            if (owner.getInventory().getContents()[i] != null && owner.getInventory().getContents()[i].equals(item)) {
                slot = i;
                break;
            }
        }
        if (slot != -1)
            owner.getInventory().getContents()[slot] = item;
        else owner.getInventory().addItem(item);
        owner.updateInventory();
        SpigotPhone.getInstance().getPhones().add(this);
        SpigotPhone.getInstance().reloadConfig();
        List<String> phonesConfig = SpigotPhone.getInstance().getConfig().getStringList("phone");
        phonesConfig.add(this.toString());
        SpigotPhone.getInstance().getConfig().set("phone", phonesConfig);
        SpigotPhone.getInstance().saveConfig();
    }


    public Phone(String fromString) {
        List<String> data = Arrays.asList(fromString.split(":: "));
        owner = Bukkit.getPlayer(UUID.fromString(data.get(0).split("=")[1]));
        battery = Integer.parseInt(data.get(1).split("=")[1]);
        passcodeEnabled = Boolean.parseBoolean(data.get(2).split("=")[1]);
        passcode = Integer.parseInt(data.get(3).split("=")[1]);
        addressBook = data.get(4).split("=")[1].equalsIgnoreCase("[]") ? new ArrayList<>() : new ArrayList<AddressBookPlayer>() {{
            String addressBookString = data.get(4).split("=")[1].substring(1).substring(data.get(4).split("=")[1].substring(1).length() - 1, data.get(4).split("=")[1].substring(1).length());
            ArrayList<String> abp = new ArrayList<>(Arrays.asList(addressBookString.split("}\",\"")));
            abp.set(0, abp.get(0).substring(1));
            abp.set(abp.size() - 1, abp.get(abp.size() - 1).substring(abp.get(abp.size() - 1).length() - 2, abp.get(abp.size() - 1).length()));
            abp.forEach((a) -> a = a.split(Pattern.quote("AddressBookPlayer{"))[1]);
            abp.forEach((string) -> add(new AddressBookPlayer(string.split(";;")[0].split("=")[1], string.split(";;")[1].split("=")[1], new ArrayList<String>() {{
                String notes = string.split(";;")[2].split("=")[1];
                if (!notes.equalsIgnoreCase("[]")) {
                    notes = notes.split(Pattern.quote("[\""), 2)[0];
                    notes = notes.split(Pattern.quote("\"]"))[notes.split(Pattern.quote("]")).length - 1];
                    addAll(Arrays.asList(notes.split("\", \"")));
                }
            }})));
        }};
        fingerPrintScanner = Boolean.parseBoolean(data.get(5).split("=")[1]);
        fingerPrintScannerEnabled = Boolean.parseBoolean(data.get(6).split("=")[1]);
        simCard = data.get(7).split("=")[1].equalsIgnoreCase("null") ? null : new SIM(data.get(7).split("=")[1].split(Pattern.quote("SIM{"))[1].split(Pattern.quote("}"))[0].split("//")[0], data.get(7).split("=")[1].split(Pattern.quote("SIM{"))[1].split(Pattern.quote("}"))[0].split("//")[1]);
        locked = Boolean.parseBoolean(data.get(8).split("=")[1]);
        service = (short) Integer.parseInt(data.get(9).split("=")[1]);
        id = Integer.parseInt(data.get(10).split("=")[1].split(Pattern.quote("}"))[0]);
        updatePhone();
        SpigotPhone.getInstance().getPhones().add(this);
        SpigotPhone.getInstance().getPhones().forEach((phone) -> {
            if (phone.getId() > SpigotPhone.CURRENT_ID) SpigotPhone.CURRENT_ID = phone.getId();
        });
    }

    public Player getOwner() {
        return owner;
    }

    public int getBattery() {
        return battery;
    }

    public List<AddressBookPlayer> getAddressBook() {
        return addressBook;
    }

    public boolean isFingerPrintScanner() {
        return fingerPrintScanner;
    }

    public ItemStack getItem() {
        return item;
    }

    public boolean isPasscodeEnabled() {
        return passcodeEnabled;
    }

    public void setPasscodeEnabled(boolean passcodeEnabled) {
        this.passcodeEnabled = passcodeEnabled;
    }

    public int getPasscode() {
        return passcode;
    }

    public void setPasscode(int passcode) {
        this.passcode = passcode;
    }

    public boolean isFingerPrintScannerEnabled() {
        return fingerPrintScannerEnabled;
    }

    public void setFingerPrintScannerEnabled(boolean fingerPrintScannerEnabled) {
        this.fingerPrintScannerEnabled = fingerPrintScannerEnabled;
    }

    public SIM getSimCard() {
        return simCard;
    }

    public void setSimCard(SIM simCard) {
        this.simCard = simCard;
    }

    public void decreaseBattery() {
        battery -= 1;
    }

    public void addAddressBookEntry(String name, String number, ArrayList<String> notes) {
        addressBook.add(new AddressBookPlayer(name, number, notes));
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public short getService() {
        return service;
    }

    public void calculateService() {

    }

    public int getId() {
        return id;
    }

    public void updatePhone() {
        item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0);
        ItemMeta iM = item.getItemMeta();
        iM.setDisplayName(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + owner.getName() + ChatColor.RESET + ChatColor.DARK_AQUA + "'s Spigot Phone");
        iM.setLore(new ArrayList<String>() {{
            if (simCard != null) add(ChatColor.GRAY + "Number: " + simCard.getNumber());
            add(ChatColor.GRAY + "Battery: " + battery);
            add(ChatColor.GRAY + "Service: " + service);
            if (locked) add(ChatColor.RED + "Locked");
        }});
        iM.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        iM.addEnchant(Enchantment.DURABILITY, id, true);
        item.setItemMeta(iM);
    }

    @Override
    public String toString() {
        ArrayList<String> addressBookString = new ArrayList<String>() {{
            addressBook.forEach((addressBookPlayer) -> add(addressBookPlayer.toString()));
        }};
        return "Phone{" +
            "owner=" + owner.getUniqueId() +
            ":: battery=" + battery +
            ":: passcodeEnabled=" + passcodeEnabled +
            ":: passcode=" + passcode +
            ":: addressBook=" + addressBookString +
            ":: fingerPrintScanner=" + fingerPrintScanner +
            ":: fingerPrintScannerEnabled=" + fingerPrintScannerEnabled +
            ":: simCard=" + (simCard != null ? simCard.toString() : "null") +
            ":: locked=" + locked +
            ":: service=" + service +
            ":: id=" + id +
            '}';
    }

    public Inventory getInventory() {
        updateInventory();
        return inv;
    }

    private void updateInventory() {
        inv = Bukkit.createInventory(null, 54, getOwner().getName() + "'s Spigot Phone");
        for (int i = 11; i < 39; i++) {
            switch (i) {
                case 11:
                    for (int ii = 0; ii < 5; ii++)
                        inv.setItem(i + ii, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
                    break;
                case 20:
                    for (int ii = 0; ii < 5; ii++)
                        inv.setItem(i + ii, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
                    break;
                case 29:
                    for (int ii = 0; ii < 5; ii++)
                        inv.setItem(i + ii, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
                    break;
                case 38:
                    for (int ii = 0; ii < 5; ii++)
                        inv.setItem(i + ii, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0));
                    break;
            }
        }

        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
        skullMeta.setOwningPlayer(getOwner());
        skullMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "" + getOwner().getName() + "'s Spigot Phone");
        skullMeta.setLore(new ArrayList<String>(){{
            add(ChatColor.GRAY + "Not Yours?");
            add(ChatColor.GRAY + "Try looking if they are online and");
            add(ChatColor.GRAY + "hand it back to them!");
        }});
        skull.setItemMeta(skullMeta);
        inv.setItem(13, skull);
    }

    private class SIM {
        private String number;
        private String data;

        public SIM(String number, String data) {
            this.number = number;
            this.data = data;
        }

        public String getNumber() {
            return number;
        }

        public String getData() {
            return data;
        }

        @Override
        public String toString() {
            return "SIM{" +
                "number=" + number +
                "// data=" + data +
                '}';


        }
    }

    private class AddressBookPlayer {
        private String name;
        private String number;
        private ArrayList<String> notes;

        public AddressBookPlayer(String name, String number, ArrayList<String> notes) {
            this.name = name;
            this.number = number;
            this.notes = notes;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public ArrayList<String> getNotes() {
            return notes;
        }

        public void setNotes(ArrayList<String> notes) {
            this.notes = notes;
        }

        @Override
        public String toString() {
            return "AddressBookPlayer{" +
                "name=" + name +
                ";; number=" + number +
                ";; notes=" + notes +
                '}';
        }
    }


}
