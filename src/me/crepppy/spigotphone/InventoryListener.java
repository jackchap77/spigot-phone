package me.crepppy.spigotphone;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryListener implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if(e.getCurrentItem() == null) return;
        SpigotPhone.getInstance().getPhones().forEach((phone) -> {
            e.setCancelled(true);
            if (e.getClickedInventory().getName().equals(phone.getInventory().getName())) {
                e.setCancelled(true);
                return;
            } else e.setCancelled(false);
        });
    }
}
