package me.crepppy.spigotphone;

import me.crepppy.spigotphone.commands.PhoneCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class SpigotPhone extends JavaPlugin {
    public static int CURRENT_ID = 1;
    private static SpigotPhone instance;
    private ArrayList<Phone> phones = new ArrayList<>();

    public static SpigotPhone getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        getConfig().getStringList("phone").forEach((phoneA) -> new Phone(phoneA));

        Bukkit.getPluginCommand("phones").setExecutor(new PhoneCommand());
        Bukkit.getPluginManager().registerEvents(new PhoneListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);
        Bukkit.getPluginManager().registerEvents(new PhoneCommand(), this);
    }

    public void onDisable() {
        getConfig().set("phone", new ArrayList<String>() {{
            phones.forEach((phone) -> add(phone.toString()));
        }});
        saveConfig();
    }

    public ArrayList<Phone> getPhones() {
        return phones;
    }
}
