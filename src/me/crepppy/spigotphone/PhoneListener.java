package me.crepppy.spigotphone;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PhoneListener implements Listener {
    private HashMap<UUID, ArrayList<ItemStack>> itemToAdd = new HashMap<>();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerClick(PlayerInteractEvent e) {
        e.setCancelled(true);
        SpigotPhone.getInstance().getPhones().forEach((phone) -> {
            ItemStack i = e.getPlayer().getInventory().getItemInMainHand().clone();
            i.setAmount(1);
                if (i.equals(phone.getItem())) {
                    if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        e.setCancelled(true);
                    }
                    phone.getOwner().openInventory(phone.getInventory());
                    return;
                }
            }
        );
        e.setCancelled(false);
    }

    @EventHandler
    public void onPlayerDrop(PlayerDropItemEvent e) {
        SpigotPhone.getInstance().getPhones().forEach((phone) -> {
            ItemStack i = e.getItemDrop().getItemStack().clone();
            i.setAmount(1);
            if (i.equals(phone.getItem())) {
                e.setCancelled(true);
            }
        });
    }

    @EventHandler
    public void onPlayerMovePhoneFromInventory(InventoryMoveItemEvent e) {
        SpigotPhone.getInstance().getPhones().forEach((phone) -> {
            ItemStack i = e.getItem().clone();
            i.setAmount(1);
            if (i.equals(phone.getItem())) {
                e.setCancelled(true);
            }
        });
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        if (e.getKeepInventory()) return;
        SpigotPhone.getInstance().getPhones().forEach((phone) -> {
            List<ItemStack> inv = new ArrayList<>();
            for(ItemStack ie : e.getEntity().getInventory()) {
                ItemStack temp = ie.clone();
                temp.setAmount(1);
                inv.add(temp);
            }
            if (inv.contains(phone.getItem())) {
                e.getDrops().remove(phone.getItem());
                if (itemToAdd.keySet().contains(e.getEntity().getUniqueId())) {
                    ArrayList<ItemStack> item = itemToAdd.get(e.getEntity().getUniqueId());
                    item.add(phone.getItem());
                    itemToAdd.replace(e.getEntity().getUniqueId(), item);
                } else {
                    itemToAdd.put(e.getEntity().getUniqueId(), new ArrayList<ItemStack>() {{
                        add(phone.getItem());
                    }});
                }
            }
        });
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        if (itemToAdd.containsKey(e.getPlayer().getUniqueId())) {
            itemToAdd.get(e.getPlayer().getUniqueId()).forEach((i) ->
                e.getPlayer().getInventory().addItem(i)
            );
            itemToAdd.remove(e.getPlayer().getUniqueId());
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (itemToAdd.containsKey(e.getPlayer().getUniqueId())) {
            itemToAdd.get(e.getPlayer().getUniqueId()).forEach((i) ->
                e.getPlayer().getInventory().addItem(i)
            );
            itemToAdd.remove(e.getPlayer().getUniqueId());
        }
    }
 }
