package me.crepppy.spigotphone.commands;

import me.crepppy.spigotphone.Phone;
import me.crepppy.spigotphone.SpigotPhone;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class PhoneCommand implements CommandExecutor, Listener {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("phones")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "You must be a player to do this.");
                return true;
            }
            Player p = (Player) sender;
            if (!p.hasPermission("spigotphone.op")) {
                p.sendMessage(ChatColor.RED + "You don't have the permission to do this.");
                return true;
            }
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("reload")) {
                    SpigotPhone.getInstance().reloadConfig();
                    SpigotPhone.getInstance().getPhones().clear();
                    SpigotPhone.getInstance().getConfig().getStringList("phone").forEach((phoneA) -> new Phone(phoneA));
                    p.sendMessage(ChatColor.GREEN + "Config reloaded");
                    return true;
                }
            }
            Inventory inv = Bukkit.createInventory(null, 27, "Spigot Phone");
            inv.setItem(12, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 13) {{
                ItemMeta im = getItemMeta();
                im.setDisplayName(ChatColor.GREEN + "Give Phone");
                im.setLore(new ArrayList<String>() {{
                    add(ChatColor.GRAY + "Gives you a phone for free with a finger print scanner");
                }});
                setItemMeta(im);
            }});

            inv.setItem(14, new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 4) {{
                ItemMeta im = getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "List Phones");
                im.setLore(new ArrayList<String>() {{
                    add(ChatColor.GRAY + "Lists all phones.");
                    add(ChatColor.GREEN + "- Right Click " + ChatColor.GRAY + "Gives phone");
                    add(ChatColor.GREEN + "- Left Click " + ChatColor.GRAY + "Removes phone");
                }});
                setItemMeta(im);
            }});
            p.openInventory(inv);

        }
        return true;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if(e.getCurrentItem() == null) return;
        if (e.getClickedInventory().getName().equals("Spigot Phone")) {
            if (!(e.getWhoClicked() instanceof Player)) return;
            e.setCancelled(true);
            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Give Phone")) {
                new Phone((Player) e.getWhoClicked(), true);
                return;
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "List Phones")) {
                Inventory inv = Bukkit.createInventory(null, 54, "Phone List");
                inv.setItem(53, new ItemStack(Material.BARRIER) {{
                    ItemMeta im = getItemMeta();
                    im.setDisplayName(ChatColor.RED + "Back");
                    setItemMeta(im);
                }});
                for (int i = 0; i < SpigotPhone.getInstance().getPhones().size(); i++) {
                    ItemStack newItem = SpigotPhone.getInstance().getPhones().get(i).getItem().clone();
                    ItemMeta im = newItem.getItemMeta();
                    int id = SpigotPhone.getInstance().getPhones().get(i).getId();
                    im.setLore(new ArrayList<String>(im.getLore()) {{
                        add(ChatColor.WHITE + " ");
                        add(ChatColor.GREEN + "- Right Click " + ChatColor.GRAY + "Gives phone");
                        add(ChatColor.GREEN + "- Left Click " + ChatColor.GRAY + "Removes phone");
                        add(ChatColor.WHITE + " ");
                        add(ChatColor.GRAY + "ID: " + id);
                    }});
                    newItem.setItemMeta(im);
                    inv.setItem(i, newItem);
                }
                e.getWhoClicked().openInventory(inv);
                return;
            }


        }
        if (e.getClickedInventory().getName().equals("Phone List")) {
            if (!(e.getWhoClicked() instanceof Player)) return;
            e.setCancelled(true);
            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED + "Back")) {
                e.getWhoClicked().closeInventory();
                ((Player) e.getWhoClicked()).performCommand("phones");
                return;
            }
            if(e.getClick() == ClickType.LEFT) {
                SpigotPhone.getInstance().getPhones().forEach((phone -> {
                    if(Integer.parseInt(e.getCurrentItem().getItemMeta().getLore().get(e.getCurrentItem().getItemMeta().getLore().size()-1).split(":")[1].trim()) == phone.getId()) {
                        SpigotPhone.getInstance().getConfig().getStringList("phone").forEach((p) -> {
                            SpigotPhone.getInstance().reloadConfig();
                            List<String> phone1 = SpigotPhone.getInstance().getConfig().getStringList("phone");
                            phone1.remove(phone.toString());
                            SpigotPhone.getInstance().getConfig().set("phone", phone1);
                            SpigotPhone.getInstance().saveConfig();
                            SpigotPhone.getInstance().getPhones().remove(phone);
                            phone.getOwner().sendMessage(ChatColor.RED + "One of your phones has been removed and will no longer work!");
                            e.getWhoClicked().sendMessage(ChatColor.GREEN + "Phone removed.");
                            return;
                        });
                    }
                }));

            } else if(e.getClick() == ClickType.RIGHT) {
                int id = Integer.parseInt(e.getCurrentItem().getItemMeta().getLore().get(e.getCurrentItem().getItemMeta().getLore().size()-1).split(":")[1].trim());
                SpigotPhone.getInstance().getPhones().forEach((phone) -> {
                    if(phone.getId() == id) {
                        e.getWhoClicked().getInventory().addItem(phone.getItem());
                        return;
                    }
                });
            }

        }
    }
}
